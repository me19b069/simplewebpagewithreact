import React, { Component } from 'react'
import { Navbar, Nav ,NavDropdown } from 'react-bootstrap';
class Navigation extends Component {
    render() {
        return (
            <Navbar collapseOnSelect expand="lg" className="navbar fixed-top" variant="dark">
                <Navbar.Brand className="nav-brand" href="/">logo</Navbar.Brand>
                <Navbar.Toggle id="menu" className="nav-toggler" aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/contact">contact</Nav.Link>
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/gallery">Gallery</Nav.Link>
                    <Nav.Link href="/blogs">Blogs</Nav.Link>
                    <NavDropdown title="Services" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="/service">Service1</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="/service">Another</NavDropdown.Item>
                    </NavDropdown>         
                    </Nav>
                    <Nav>
                    {
                        window.isLoggedIn?<Nav.Link eventKey={2} href="/userpanel">
                            UserPanel
                    </Nav.Link>:<Nav.Link eventKey={2} href="/aurthorise">
                        Login or Sign Up
                    </Nav.Link>
                    }
                    </Nav>
                </Navbar.Collapse>
                </Navbar>
        )
    }
}

export default Navigation
