import axios from 'axios';
import React, { Component } from 'react'
import UserInfo from './userinfo';
class UserPanel extends Component {

constructor(){
    super();
    this.state = {
        data:""
    }
    this.getUser= this.getUser.bind(this);
}


getUser() {
    axios.get("http://localhost/dashboard/my-things/design-stop-api/api/v1/controller/fetchuser.php?user_id=3",{
        headers:{
            "Authorization":"Token 0c80f352e302ac7506476a98b23ac528be2fa04a"
        }
    }).then(response =>{
        
        return response.data.data
    }).catch(err =>{
        console.log(err)
        return ""
    })

}


    render() {
        return (
            <div className="container  userpanel-container" user_id={window.user_id}>
               <UserInfo/>
            </div>
        )
    }
}

export default UserPanel
