const cardlist=[
    {
        "id":1,
        "blog-image":"https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(71).jpg",
        "blog-title":"Card title",
        "blog-description": "Some quick example text to build on the card title and make up the bulk of the card's content.",
        "blog-likes":5
    },
    {
        "id":2,
        "blog-image":"https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(72).jpg",
        "blog-title":"Card title",
        "blog-description": "Some quick example text to build on the card title and make up the bulk of the card's content.",
        "blog-likes":5
    },
    {
        "id":3,
        "blog-image":"https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(73).jpg",
        "blog-title":"Card title",
        "blog-description": "Some quick example text to build on the card title and make up the bulk of the card's content.",
        "blog-likes":5
    }
]

export default cardlist;