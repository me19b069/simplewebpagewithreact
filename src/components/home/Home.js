import React, { Component } from 'react'
import Footer from '../about/footer'
import "./home.css";
import LoadBlog from './loadblog';
import cardlist from './blogcardslist';

class Home extends Component {


      render() {
        return (
      
        <div>
        {/*Main layout*/}
        <main className="mt-5">
          {/*Main container*/}
          <div id="home-container" className="container">
            <div className="row">
              <div className="col-md-7 mb-4">
                <div className="view overlay z-depth-1-half">
                  <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" className="card-img-top" alt="" />
                  <div className="mask rgba-white-light" />
                </div>
              </div>
              <div className="col-md-5 mb-4">
                <h2>Some awesome heading</h2>
                <hr />
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis pariatur quod ipsum atque quam
                  dolorem
                  voluptate officia sunt placeat consectetur alias fugit cum praesentium ratione sint mollitia, perferendis
                  natus quaerat!</p>
                <a href="https://mdbootstrap.com/" className="btn btn-indigo">Get it now!</a>
              </div>
            </div>

            <div className="row">


              {/*Grid column*/}
              <div className="col-lg-4 col-md-12 mb-4">
                
                <LoadBlog carddata={cardlist[0]}/>
              </div>
              <div className="col-lg-4 col-md-6 mb-4">
                
                <LoadBlog carddata={cardlist[1]}/>
              </div>
              <div className="col-lg-4 col-md-6 mb-4">
                
                <LoadBlog carddata={cardlist[2]}/>
              </div>
              {/*Grid column*/}
            </div>
            {/*Grid row*/}
          </div>
          {/*Main container*/}
        </main>
        {/*Main layout*/}
        <Footer/>
      </div>
    
    );
      
    
    }
    }
    

export default Home;
