import React from 'react'
import BlogCard from '../blog/blogcard';



function LoadBlog({carddata}) {     
    const RenderBlog = (e,id)=>{
        //redire
        console.log(id);
        
    }
    console.log(carddata['blog-image']);

    return (

        !carddata?<div></div>
        :<BlogCard id={carddata['id']} 
        titleimage={carddata['blog-image']} 
        title={carddata['blog-title']}
        description={carddata['blog-description']}
        likes={carddata['blog-likes']}
        RenderBlog={RenderBlog}
         />
    )
}

export default LoadBlog;
