import React, { Component } from 'react'
import "./blogcard.css";
class BlogCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:this.props.id,
            titleimage:this.props.titleimage,
            title:this.props.title,
            description:this.props.description,
            likes:this.props.likes
        }
        this.viewBlog= this.viewBlog.bind(this);
    }

viewBlog(e){
    e.preventDefault();
    this.props.RenderBlog();

}

    render() {
        return (
                <div className="card">
                  {/*Card image*/}
                  <div className="card-image-container">
                    <img src={this.state.titleimage} className="card-image" alt="" />
                    {/* <a href="#">
                      <div className="mask rgba-white-slight" />
                    </a> */}
                  </div>
                  <div className="card-body">
                    <h4 className="card-title">{this.state.title}</h4>
                    <p className="card-text">{this.props.description}</p>
                    <div className="card-bottom-row">
                    <button className="read-more btn btn-indigo" onClick={this.viewBlog}>Readmore</button>
                    {/* <span className="card-likes">{this.state.likes}</span> */}
                    </div>
                    
                  </div>
                </div>
        )
    }
}

export default BlogCard
