import React, { Component } from 'react'
import "./blog.css";
import BlogCard from "./blogcard";
import cardList from "./blogcardata";
 class Blog extends Component {
    constructor(props){
        super(props);
        this.state = {
            list:'',
            pageindex: 0,
        }
    }

    componentDidMount(){
        this.setState({
            list:cardList,
        })
    }

    render() {
        return (
            <div className="blog-container">
                <div className="blog-grid">
                {cardList.map((card, index) =>{
                        return <BlogCard id={card['id']} key={index}  title={card['blog-title']} titleimage={card['blog-image']} description={card['blog-description']} />
                }
                    ) }

                </div>
                
            </div>
        )
    }
}

export default Blog;
