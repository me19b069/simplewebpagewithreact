import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router";
export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            name:"",
            password:"",
            checkbox:"",
            message:""
        }
        
        this.handleEmailChange=this.handleEmailChange.bind(this);
        this.login= this.login.bind(this);
        this.handlePasswordChange= this.handlePasswordChange.bind(this);

    }

    handleEmailChange(e){
        e.preventDefault();
        this.setState({
            name:e.target.value
        })
    }
    handlePasswordChange(e){
        
        e.preventDefault();
        this.setState({
            password:e.target.value
        })
    }

    login(e){
        e.preventDefault();
        axios.post("/login",
        ).then(response =>{
            return response.data;
        }).then(responsedata =>{
            if(responsedata.status===200){
                    window.isLoggedIn=true;
                    window.user_id=responsedata['user_id'];
                    window.token=responsedata['token'];
                    <Redirect to="/"/>
            }
        }).catch(err =>{
            console.log(err);
        })
        console.log(this.state);

        }

        handleCheckboxChange(e){
            e.preventDefault();
        }

    render() {
        
        return (
            
            <form className="form-auth">
                <h3>Sign In</h3>
                { !this.state.message?" ":<div className="form-group">
                    <label>{this.state.message}</label>
                </div>}
                
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" value={this.state.email} onChange={this.handleEmailChange} />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" value={this.state.password} onChange={this.handlePasswordChange} />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" value={this.state.checkbox} onChange={this.handleCheckboxChange} />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>

                <button  onClick={this.login} className="btn btn-primary btn-block">Submit</button>
                <p className="forgot-password text-right">
                    Forgot <a href="/">password?</a>
                </p>
            </form>
        );
    }
}
