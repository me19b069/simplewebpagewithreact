import React, { Component } from 'react'
import "./aurthrisation.css";
import Login from ".//login.component";
import SignUp from ".//signup.component";
class Aurthorisation extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             login:true,
        }
        this.login= this.login.bind(this);
        this.register=this.register.bind(this);
    }
    
        login(e){
            e.preventDefault();
            this.setState({
                login:true,
            })
        }
        register(e){
            e.preventDefault();
            this.setState({
                login:false,
            })
        }

    render() {
        const auth=this.state.login?<Login/> :<SignUp/>
        return (
            <div className="container auth-wrapper">
                <div className="row btn-container">
                    <button type="button" onClick={this.login} className="btn btn-default">Login</button>
                    <button type="button" onClick={this.register} className="btn btn-default">Register</button>
                </div>
                <div className="row auth-container">
                   {auth} 
                </div>
            </div>
        )
    }
}

export default Aurthorisation
