import React from "react";

class AccordionApp extends React.Component {
    render() {
        const title = 'FAQ s';
        const hiddenTexts = [{
            label: 'Question',
            value: 'Text of Accordion 1'
        },
        {
            label: 'Question',
            value: 'Text of Accordion 2'
        },
        {
            label: 'Question',
            value: 'Text of Accordion 3'
        },
        {
            label: 'Question',
            value: 'Text of Accordion 4'
        }];
        return (
            <div>
                <Header title={title} />
                <h1>Frequently Asked Questions</h1>
                <Accordion hiddenTexts={hiddenTexts} />
            </div>
        );
    }
}

class Header extends React.Component {
    render() {
        return (
            <h1>{this.props.title}</h1>
        );
    }
}

class Accordion extends React.Component {
    render() {
        return (
            <div className="accordion">
                {this.props.hiddenTexts.map((hiddenText) => <AccordionItem key={hiddenText.label} hiddenText={hiddenText} />)}
            </div>
        );
    }
}

class AccordionItem extends React.Component {
    
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility: false,
            icon:true,
        }
    }
    
    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility,
                icon:!prevState.icon,
            }
        })
    }

    render() {
        const activeStatus = this.state.visibility ? 'active' : ''

        return (
            <div>
                <button className="accordion__button" onClick={this.handleToggleVisibility}>{this.props.hiddenText.label}<span style={this.style} >{this.state.icon?"+":"-"}</span></button>
                <p className={`accordion__content ${activeStatus}`}>
                {
                    this.props.hiddenText.value
                }
                </p>
                
            </div>
        );
    }
}

export default AccordionApp;