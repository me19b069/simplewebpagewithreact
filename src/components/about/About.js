import React, { Component } from 'react'
import Footer from './footer';
import "./assets/css/footer.css"
import Team from './team.js';
import "./assets/css/about.css";
import Faq from './faq';
class About extends Component {
    render() {
        return (
            <>
            <Faq/>
            <div class="about-section">
            <h1>About Us Page</h1>
            <p>Some text about who we are and what we do.</p>
            <p>Resize the browser window to see that this page is responsive by the way.</p>
            <Team />
            </div>
            <Footer/>
            </>
        )
    }
}

export default About
